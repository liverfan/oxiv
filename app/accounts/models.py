# -*- coding: utf-8 -*-
import hashlib
from django.contrib.auth.models import AbstractUser
from django.db import models
import time
from django.db.models.signals import post_save


def _createPass():
    """
    Function for creating 6 symbols random hash for referral code
    """
    hash = hashlib.md5()
    hash.update(str(time.time()))
    return hash.hexdigest()[:6]


class UserProfile(AbstractUser):
    is_customer = models.BooleanField(default=False)
    is_manager = models.BooleanField(default=False)

    phone = models.CharField(max_length=255, blank=True, null=True)

    #tags добавить тэги типа ремонтдвигателей и т.п.
    balance = models.IntegerField(default=0, blank=True)
    extra_price = models.IntegerField(default=18, blank=True)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    """
    Users within the Django authentication system are represented by this
    model.

    Username, password and email are required. Other fields are optional.
    """
    def __unicode__(self):
        if self.first_name or self.last_name:
            full_name = '%s %s' % (self.first_name, self.last_name)
            return full_name.strip()
        else:
            return self.username

    class Meta(AbstractUser.Meta):
        swappable = 'AUTH_USER_MODEL'


    #get change balance ONLY email
    def get_change_password(self):
        password = _createPass()
        self.set_password(password)
        self.save()
        return str(password)

    #get balance
    def get_balance(self):
        return format(self.balance, ',d').replace(',', ' ')



"""
#
# User notification
#
"""
class UserNotification(models.Model):
    user = models.ForeignKey(UserProfile, verbose_name='User')

    email = models.EmailField(blank=True, null=True)

    noti_email = models.BooleanField(default=True, verbose_name='Email noti')
    noti_site = models.BooleanField(default=True, verbose_name='noti')
    noti_sound = models.BooleanField(default=False, verbose_name='Sound noti')

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)


"""
#
# Create user notification
#
"""
def add_noti_user(sender, **kwargs):
    user = kwargs["instance"]
    if kwargs["created"]:
        users = UserNotification.objects.create(user=user, email=user.email)

post_save.connect(add_noti_user, sender=UserProfile)