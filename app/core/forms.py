# -*- coding: utf-8 -*-
from django import forms
from core.models import UserSettings, ProductsAndService

"""
#
# FORM SIDEBAR COMPANY
#
"""
class FormSidebarCompany(forms.ModelForm):
    company_name = forms.CharField(required=True)
    phone = forms.CharField(required=True)


    class Meta:
        model = UserSettings
        fields = ('city', 'address', 'mode_job', 'site_url')
        exclude = ('user', 'topics', 'status')

"""
#
# FORM POPUP ADD COMPANY
#
"""
class FormPopupAddCompany(forms.ModelForm):

    class Meta:
        model = ProductsAndService
        exclude = ('user',)
