# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_auto_20150729_1402'),
    ]

    operations = [
        migrations.CreateModel(
            name='StatusBids',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('choice', models.CharField(max_length=200, null=True, blank=True)),
            ],
        ),
        migrations.AddField(
            model_name='usersettings',
            name='site',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='userbids',
            name='status_callback',
            field=models.ForeignKey(blank=True, to='core.StatusBids', null=True),
        ),
        migrations.AddField(
            model_name='usersettings',
            name='status',
            field=models.ManyToManyField(to='core.StatusBids'),
        ),
    ]
