# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0004_statusbids_value'),
    ]

    operations = [
        migrations.RenameField(
            model_name='usersettings',
            old_name='site',
            new_name='city',
        ),
    ]
