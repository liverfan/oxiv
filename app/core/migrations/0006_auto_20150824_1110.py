# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0005_auto_20150805_0059'),
    ]

    operations = [
        migrations.AddField(
            model_name='usersettings',
            name='address',
            field=models.CharField(max_length=1055, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='usersettings',
            name='mode_job',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='usersettings',
            name='site_url',
            field=models.URLField(null=True, blank=True),
        ),
    ]
