# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0010_auto_20150902_0309'),
    ]

    operations = [
        migrations.AddField(
            model_name='userbids',
            name='phone_callback_two',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
    ]
