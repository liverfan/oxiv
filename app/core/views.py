# -*- coding: utf-8 -*-
import hashlib
import httplib
import json
import urllib
from django.conf import settings
from django.core import serializers
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render, redirect
from django.template.loader import render_to_string
from django.views.generic import TemplateView
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from yandex_money.forms import PaymentForm
from yandex_money.models import Payment
from accounts.models import UserProfile, UserNotification
from core.api import YandexApi
from core.forms import FormSidebarCompany, FormPopupAddCompany
from core.http import JSONResponseMixin
from core.models import UserBids, Topics, UserSettings, StatusBids, ProductsAndService
from core.serializers import ProductsAndServiceSerializer, UserBidsSerializer
from payment.models import Order, HistoryDebit
from payment.serializers import OrderSerializer, HistoryDebitSerializer


def test(request):
    if request.GET['code']:
        #Формирование параметров (тела) POST-запроса с указанием кода подтверждения
        query = {
            'grant_type': 'authorization_code',
            'code': request.GET['code'],
            'client_id': '0cd94059e3ea4276b204fd744bea18ac',
            'client_secret': '1dd4715ab28d40cc9c08d8a09d7f2e5e',
        }
        query = urllib.urlencode(query)

        #Формирование заголовков POST-запроса
        header = {
            'Content-Type': 'application/x-www-form-urlencoded'
        }

        #Выполнение POST-запроса и вывод результата
        connection = httplib.HTTPSConnection('oauth.yandex.ru')
        connection.request('POST', '/token', query, header)
        response = connection.getresponse()
        result = response.read()
        connection.close()
        tmp_filename = settings.TMP_PATH + '/messages.txt'
        with open(tmp_filename, "wb") as f:
           f.write(json.loads(result)['access_token'])
        f.close()

        #Токен необходимо сохранить для использования в запросах к API Директа
        return json.loads(result)['access_token']



class HomePageView(TemplateView):
    template_name = 'registration/registration_form.html'

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_authenticated():
                return redirect('http://i.oxiv.ru/')
        else:
            return redirect('core:user_leads')
        return super(HomePageView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(HomePageView, self).get_context_data(**kwargs)
        #res = YandexApi(method='GetSummaryStat')
        #print(res.result())
        return context

"""
#
# A list of bids the user (Список заявок пользователя)
#
"""
class UserBidsListView(TemplateView):
    template_name = 'core/bids.html'

    def dispatch(self, *args, **kwargs):
        #if not self.request.user.is_authenticated():
                #return redirect('auth_login')
        return super(UserBidsListView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        context = self.get_context_data()
        GET = self.request.GET

        if 'type' in GET:
            if GET['value'] == 'lead':
                lead_id = GET.get('lead', None)

                item = UserBids.objects.get(id=lead_id, user=self.request.user)
                content = JSONResponseMixin(UserBidsSerializer(item).data)

                return content
        else:
            return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        context = super(UserBidsListView, self).get_context_data(**kwargs)
        context['user_bids'] = UserBids.objects.filter(user=self.request.user)
        context['settings'] = UserSettings.objects.get(user=self.request.user)
        return context

"""
#
# A statistics' the user (Статистика пользователя)
#
"""
class UserStatisticsView(TemplateView):
    template_name = 'core/statistics.html'

    def dispatch(self, *args, **kwargs):
        #if not self.request.user.is_authenticated():
                #return redirect('auth_login')
        return super(UserStatisticsView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(UserStatisticsView, self).get_context_data(**kwargs)
        return context


"""
#
# The profile user (Профиль пользователя)
#
"""
class UserProfileView(TemplateView):
    template_name = 'core/profile.html'

    def dispatch(self, *args, **kwargs):
        #if not self.request.user.is_authenticated():
                #return redirect('auth_login')
        return super(UserProfileView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        context = self.get_context_data()
        GET = self.request.GET

        if 'type' in GET:
            if GET['value'] == 'product':
                product_id = GET.get('product', None)

                item = ProductsAndService.objects.get(id=product_id, user=self.request.user)
                content = JSONResponseMixin(ProductsAndServiceSerializer(item).data)

                return content
        else:
            return self.render_to_response(context)



    def get_context_data(self, **kwargs):
        context = super(UserProfileView, self).get_context_data(**kwargs)
        obj = UserSettings.objects.get(user=self.request.user)

        initial_frmsc = {
            'company_name': self.request.user.first_name,
            'phone': self.request.user.phone,
        }
        context['frmsc'] = FormSidebarCompany(instance=obj, initial=initial_frmsc)
        context['frmpac'] = FormPopupAddCompany()
        context['products_items'] = ProductsAndService.objects.filter(user=self.request.user).order_by('-created')
        return context

"""
#
# VIEWS FORM SIDEBAR COMPANY
#
"""
def company_form_sidebar(request):
    context = {}
    obj = UserSettings.objects.get(user=request.user)

    if request.POST:
        form = FormSidebarCompany(request.POST, instance=obj)
        if form.is_valid():
            frm = form.save(commit=False)
            frm.user = request.user
            frm.save()

            user = UserProfile.objects.get(id=request.user.id)
            user.first_name = form.cleaned_data.get('company_name', None)
            user.phone = form.cleaned_data.get('phone', None)
            user.save()

            context['result'] = 'success'
        else:
            context = dict(form.errors.items())

    return JsonResponse(context)

"""
#
# VIEWS FORM POPUP aDD COMPANY
#
"""
def company_form_popup_add(request):
    context = {}
    data = {}

    if request.user.is_anonymous():
        return False

    if request.POST:
        if 'id' in request.POST and 'change' in request.POST:
            item = ProductsAndService.objects.get(id=request.POST['id'], user=request.user)
            form = FormPopupAddCompany(request.POST, instance=item)
            if form.is_valid():
                frm = form.save(commit=False)
                frm.user = request.user
                frm.save()

                if 'image' in request.FILES:
                    frm.image = request.FILES['image']
                    frm.save()

                context['result'] = 'success'
                data['item'] = frm
                context['id'] = frm.id
                context['html'] = render_to_string("core/profile-product-only.html", data)
            else:
                context = dict(form.errors.items())
        if 'id' not in request.POST:
            form = FormPopupAddCompany(request.POST)
            if form.is_valid():
                frm = form.save(commit=False)
                frm.user = request.user
                frm.is_active = False
                frm.save()

                if 'image' in request.FILES:
                    frm.image = request.FILES['image']
                    frm.save()

                context['result'] = 'success'
                data['item'] = frm
                context['id'] = frm.id
                context['html'] = render_to_string("core/profile-product-only.html", data)
            else:
                context = dict(form.errors.items())
    elif request.GET:
        type = request.GET.get('type', None)
        id = request.GET.get('id', None)

        if type == 'confirm':
            item = ProductsAndService.objects.get(id=id, user=request.user)
            item.is_active = True
            item.save()
        elif type == 'delete':
            ProductsAndService.objects.get(id=id, user=request.user).delete()

        elif type == 'confrim_edit':
            pass

    return JsonResponse(context)
"""
#
# Save commentary bid list user
#
"""
def user_bid_commentary_save(request, id):
    print(id)
    return  json.dumps({'success:true'})



"""
#
# PAGE BALANCE СТРАНИЦА БАЛАНСА
#
"""

"""
#
# PAGE BALANCE (Страница баланса)
#
"""
class UserBalanceView(TemplateView):
    template_name = 'core/balance.html'

    def dispatch(self, *args, **kwargs):
        #if not self.request.user.is_authenticated():
                #return redirect('auth_login')
        return super(UserBalanceView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        context = self.get_context_data()
        GET = self.request.GET
        print(GET.items())

        if 'type' in GET:
            if GET['type'] == 'order':
                count = int(GET.get('count', None)) - 1
                count_end = count + 6
                last = False

                if Order.objects.filter(user=self.request.user)[count_end:count_end+1]:
                    last = True

                items = Order.objects.filter(user=self.request.user)[count:count_end]
                result = [OrderSerializer(item).data for item in items]
                result.append({'last': last})

            elif GET['type'] == 'debit':
                count = int(GET.get('count', None)) - 1
                count_end = count + 6
                last = False

                if HistoryDebit.objects.filter(user=self.request.user)[count_end:count_end+1]:
                    last = True

                items = HistoryDebit.objects.filter(user=self.request.user)[count:count_end]
                result = [HistoryDebitSerializer(item).data for item in items]
                result.append({'last': last})

            content = JSONResponseMixin(result)
            return content
        else:
            return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        context = super(UserBalanceView, self).get_context_data(**kwargs)

        context['orders'] = Order.objects.filter(user=self.request.user)[:6]
        context['debits'] = HistoryDebit.objects.filter(user=self.request.user)[:6]
        return context

"""
#
# PAGE Order(Страница баланса)
#
"""
def order_page(request):
    context = {}
    data = {}
    #template_name = 'core/order-page.html'
    if request.POST:
        payment = Payment(user=request.user, order_amount=request.POST['vMin'])
        payment.save()
        data['form'] = PaymentForm(instance=payment)
        context['html'] = render_to_string("core/order-page.html", data)

    return JsonResponse(context)


""""
    def get_context_data(self, **kwargs):
        payment = Payment(order_amount=123)
        payment.save()

        ctx = super(OrderPage, self).get_context_data(**kwargs)
        ctx['form'] = PaymentForm(instance=payment)
        return ctx
"""

"""
#
# PAGE SETTINGS СТРАНИЦА НАСТРОЕК
#
"""

"""
#
# Settings user (Настройки пользователя)
#
"""
class UserSettingsView(TemplateView):
    template_name = 'core/settings.html'

    def dispatch(self, *args, **kwargs):
        #if not self.request.user.is_authenticated():
                #return redirect('auth_login')
        return super(UserSettingsView, self).dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        context = self.get_context_data()
        settings = context['settings']
        settings_noti = context['noti_settings']

        if 'city' in self.request.POST:
            city = self.request.POST.get('city', None)
            topics = self.request.POST.get('topics', None)

            settings.city = city
            settings.save()

            if topics:
                for t in topics:
                    settings.topics.add(Topics.objects.get(id=t))
                    settings.save()
            else:
                for t in settings.topics.all():
                     settings.topics.remove(t)
                     settings.save()

        elif '1' in self.request.POST:
            for rst in self.request.POST.items():
                try:
                    status = settings.status.get(value=rst[0])
                    status.choice = rst[1]
                    status.save()
                except:
                    pass
            context['active'] = 'status'

        elif 'email' in request.POST:
            settings_noti.noti_email = self.request.POST.get('noti_email', None)
            settings_noti.noti_site = self.request.POST.get('noti_site', None)
            settings_noti.noti_sound = self.request.POST.get('noti_sound', None)
            settings_noti.email = self.request.POST.get('email', None)
            context['active'] = 'noti'

        return super(UserSettingsView, self).render_to_response(context)

    def get_context_data(self, **kwargs):
        context = super(UserSettingsView, self).get_context_data(**kwargs)
        context['topics'] = Topics.objects.filter(is_active=True)
        context['settings'] = UserSettings.objects.get(user=self.request.user)
        context['noti_settings'] = UserNotification.objects.get(user=self.request.user)
        context['active'] = 'bids'
        return context


def test_aviso(request):
    str = '<paymentAvisoResponse performedDatetime="2011-05-04T20:38:11.000+04:00" code="0" invoiceId="1234567" shopId="13"/>'
    #xml = render_to_string('xml_template.xml', {'str': str})
    POST = request.POST
    m = hashlib.md5()
    #$hash = md5($_.';'.$_.';'.$_.';'.$_.';'.$configs['shopId'].';'.$_.';'.$_POST['customerNumber'].';'.$configs['ShopPassword']);
    m.update(POST['action']+';'+POST['orderSumAmount']+';'+POST['orderSumCurrencyPaycash']+';'+POST['orderSumBankPaycash']+';'+str(settings.YANDEX_MONEY_SHOP_ID)+';'+POST['invoiceId']+';'+POST['customerNumber']+';'+str(settings.YANDEX_MONEY_SHOP_PASSWORD))
    hash = str(m.hexdigest())
    if hash.lower() != POST['md5'].lower():
        code = 1
    else:
        code = 0

    return render(request, 'xml_template.xml', {"code": code,
                                                "date": POST['requestDatetime'],
                                                "invoice": POST['invoiceId'],
                                                "shop_id": settings.YANDEX_MONEY_SHOP_ID}, content_type="application/xhtml+xml")

def test_check_order(request):
    str = '<paymentAvisoResponse performedDatetime="2011-05-04T20:38:11.000+04:00" code="0" invoiceId="1234567" shopId="13"/>'
    #xml = render_to_string('xml_template.xml', {'str': str})
    POST = request.POST
    m = hashlib.md5()
    #$hash = md5($_.';'.$_.';'.$_.';'.$_.';'.$configs['shopId'].';'.$_.';'.$_POST['customerNumber'].';'.$configs['ShopPassword']);
    m.update(POST['action']+';'+POST['orderSumAmount']+';'+POST['orderSumCurrencyPaycash']+';'+POST['orderSumBankPaycash']+';'+str(settings.YANDEX_MONEY_SHOP_ID)+';'+POST['invoiceId']+';'+POST['customerNumber']+';'+POST['customerNumber']+';'+str(settings.YANDEX_MONEY_SHOP_PASSWORD))
    hash = str(m.hexdigest())
    if hash.lower() != POST['md5'].lower():
        code = 1
    else:
        code = 0

    return render(request, 'xml_template_two.xml', {"code": code,
                                                "date": POST['requestDatetime'],
                                                "invoice": POST['invoiceId'],
                                                "shop_id": settings.YANDEX_MONEY_SHOP_ID}, content_type="application/xhtml+xml")