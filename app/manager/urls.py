from django.conf.urls import patterns, url
from manager.views import *

urlpatterns = patterns('',
    url(r'^$', ManagerUserListView.as_view(), name='manager_users'),
    url(r'^statistics/$', ManagerStatisticsView.as_view(), name='manager_statistics'),
    url(r'^balance/$', ManagerBalanceView.as_view(), name='manager_balance'),
    url(r'^settings/$', ManagerSettingsView.as_view(), name='manager_settings'),


    #url(r'^bids/$', UserBidsListView.as_view(), name='user_bids'),


)