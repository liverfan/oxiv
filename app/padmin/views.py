# -*- coding: utf-8 -*-
from django.views.generic import TemplateView
from core.models import UserBids, UserSettings, Bids

"""
#
# PAGE ADMIN USERS
#
"""

"""
#
# Admin user list (Список пользователя)
#
"""
class AdminUserListView(TemplateView):
    template_name = 'administrator/users.html'

    def dispatch(self, *args, **kwargs):
        #if not self.request.user.is_authenticated():
                #return redirect('auth_login')
        return super(AdminUserListView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AdminUserListView, self).get_context_data(**kwargs)
        return context



"""
#
# PAGE ADMIN STATISTIC
#
"""


"""
#
# A statistics' the padmin (Полная статистика администратора )
#
"""
class AdminStatisticsView(TemplateView):
    template_name = 'administrator/statistics.html'

    def dispatch(self, *args, **kwargs):
        #if not self.request.user.is_authenticated():
                #return redirect('auth_login')
        return super(AdminStatisticsView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AdminStatisticsView, self).get_context_data(**kwargs)
        return context


"""
#
# PAGE ADMIN SETTINGS
#
"""

"""
#
# Settings padmin (Настройки пользователя)
#
"""
class AdminSettingsView(TemplateView):
    template_name = 'administrator/settings.html'

    def dispatch(self, *args, **kwargs):
        #if not self.request.user.is_authenticated():
                #return redirect('auth_login')
        return super(AdminSettingsView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AdminSettingsView, self).get_context_data(**kwargs)
        return context


"""
#
# PAGE LEADS STATISTIC
#
"""

"""
#
# The leads padmin (страница заявок)
#
"""
class AdminLeadsView(TemplateView):
    template_name = 'administrator/leads.html'

    def dispatch(self, *args, **kwargs):
        #if not self.request.user.is_authenticated():
                #return redirect('auth_login')
        return super(AdminLeadsView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AdminLeadsView, self).get_context_data(**kwargs)
        context['leads'] = Bids.objects.all()
        context['settings'] = UserSettings.objects.get(user=self.request.user)
        return context


"""
#
# PAGE PARTNERS STATISTIC
#
"""

"""
#
# The partnres padmin (страница партнеров)
#
"""
class AdminPartnersView(TemplateView):
    template_name = 'administrator/partners.html'

    def dispatch(self, *args, **kwargs):
        #if not self.request.user.is_authenticated():
                #return redirect('auth_login')
        return super(AdminPartnersView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AdminPartnersView, self).get_context_data(**kwargs)
        return context