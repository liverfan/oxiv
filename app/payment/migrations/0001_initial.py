# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('core', '0008_promocode'),
    ]

    operations = [
        migrations.CreateModel(
            name='HistoryDebit',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amount', models.PositiveIntegerField(verbose_name='\u0421\u0443\u043c\u043c\u0430 \u0441\u043f\u0438\u0441\u0430\u043d\u0438\u044f')),
                ('check', models.BooleanField(default=False)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('leads', models.ForeignKey(related_name='historydebit', to='core.UserBids')),
                ('user', models.ForeignKey(related_name='historydebit_user', verbose_name='\u044e\u0437\u0435\u0440', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amount', models.PositiveIntegerField(verbose_name='\u0421\u0443\u043c\u043c\u0430 \u0432 \u043e\u043f\u043b\u0430\u0442\u044b')),
                ('check', models.BooleanField(default=False)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('promocode', models.ForeignKey(related_name='order_promocode', blank=True, to='core.Promocode', null=True)),
                ('user', models.ForeignKey(related_name='order_user', verbose_name='\u044e\u0437\u0435\u0440', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
