from rest_framework import serializers
from core.serializers import UserBidsSerializer
from payment.models import Order, HistoryDebit


class OrderSerializer(serializers.ModelSerializer):
    pk = serializers.IntegerField(read_only=True)

    date_at = serializers.SerializerMethodField('date')
    time_at = serializers.SerializerMethodField('time')

    def date(self, obj):
        return obj.created.strftime("%d.%m.%Y")

    def time(self, obj):
        return obj.created.strftime("%I:%M")


    class Meta:
        model = Order
        fields = ('pk', 'date_at', 'time_at', 'promocode', 'amount', 'created', 'updated',)

class HistoryDebitSerializer(serializers.ModelSerializer):
    pk = serializers.IntegerField(read_only=True)
    leads = UserBidsSerializer(many=False, read_only=True)

    date_at = serializers.SerializerMethodField('date')
    time_at = serializers.SerializerMethodField('time')

    def date(self, obj):
        return obj.created.strftime("%d.%m.%Y")

    def time(self, obj):
        return obj.created.strftime("%I:%M")

    class Meta:
        model = HistoryDebit
        fields = ('pk', 'date_at', 'time_at', 'leads', 'amount', 'created', 'updated',)