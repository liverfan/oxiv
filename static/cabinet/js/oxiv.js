/**
 * Created by Andronov Andrey on 24.08.2015.
 */
/*
var AddToTemplateProducts = function() {
    alertify.okBtn("Да").cancelBtn("Нет")
    .confirm("Вы действительно хотите добавить товар/услугу?", function (event) {
        event.preventDefault();
        alertify.success("Вы добавили");
    }, function(event) {
        event.preventDefault();
        alertify.error("Отмена");
    });
};  */

var AlertMessage = function(name_id, id, data) {
    var name, text, func, func_delete;
    var action = $('#'+name_id).attr('action');

    switch(name_id){
        case 'FormPopupAddCompany':
            name = name_id;
            text = "Вы действительно хотите добавить товар/услугу?";
            func = function(data){
                $.get(action, {type: "confirm", id: id});
                $('.btn-profile-edit-adv:first').before(data);
            };
            func_delete = function(data){
                $.get(action, {type: "delete", id: id});
            };
            break;
        case 'FormPopupEditCompany':
            name = name_id;
            text = "Вы действительно хотите изменить товар/услугу?";
            func = function(data){
                $('#'+name_id).append('<input style="display:none" value="0" name="change">');
                ajaxifyCallbacks.ajax.FormPopupEditCompany('', name_id, data);
            };
            func_delete = function(data){
                //$.get(action, {type: "delete", id: id});
            };
            break;
        case 'FormPopupEditCompany_DEL':
            name = name_id;
            text = "Вы действительно хотите удалить товар/услугу?";
            func = function(data){
                $.get(data, {type: "delete", id: id});
                $('[data-id="'+id+'"]').remove();
            };
            func_delete = function(data){
                //$.get(action, {type: "delete", id: id});
            };
            break
    }
    console.log(name);

    alertify.okBtn("Да").cancelBtn("Нет")
    .confirm(text, function (event) {
        if (event) {func(data);}
        event.preventDefault();
        alertify.success("Вы добавили");
    }, function(event) {
        if (event) {func_delete(data);}
        event.preventDefault();
        alertify.error("Отмена");
    });
};

var funcSerializeFormToObject = function(form){
    console.log('func', form);
        var formData = new FormData(),
                params   = form.serializeArray(),
                files    = form.find('[name="image"]')[0].files;

        $.each(files, function(i, file) {
                formData.append('image', file);
        });

        $.each(params, function(i, val) {

                formData.append(val.name, val.value);
        });

      return formData;
};

var ajaxifyCallbacks = function () {
  return {
    //Submit callbacks
    submit: {
        //Default submit callback
        defaultCallback: function (e, form, data) {
            var trigger = form.find('.submit-trigger');
            trigger.on('hidden.bs.tooltip', function () {
                trigger.tooltip('destroy');
            });
            trigger.tooltip({title: 'Сохранено', trigger: 'manual', html: true, placement: 'left'}).tooltip('show');
            setTimeout(function () {
                trigger.tooltip('hide');
            }, 3000);
        },
        FormSidebarCompany: function (e, form, data) {
            console.log(data);
            console.log(data.result);
            if(data.result){
                later();
                $('.profile-form-content').toggleClass('active');
                $('.profile-form-edit').css({'margin-bottom': '0px'}).toggleClass('active');

            }

            var later = function() {
                var elements = $(form).find('.form-control');
                $(elements).each(function (indx, element) {
                    console.log();
                    var name = $(element).attr('name');
                    var val = $(element).val();
                    var icon = '<i class="'+$($(element).parent().get(0)).next().find('i').attr('class')+'" aria-hidden="true"></i>';
                    var a_link = $($(element).parent().get(0)).next().find('a').html();

                    if(a_link != undefined){
                        var link = '<a href="'+val+'">'+val+'</a>';
                        $($(element).parent().get(0)).next().html(icon + link)
                    }
                    else{
                        $($(element).parent().get(0)).next().html(icon + val)
                    }
                    console.log($($(element).parent().get(0)).next().html());
                });
            };

            /*
            if (data.errors) {
                console.log(data)

                form.prepend('<div class="alert alert-danger fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Закрыть</span></button>Форма заполнена неверно</div>');
                $('html,body').animate({scrollTop: form.offset().top - $('.block-top-panel').height()}, 500);
            } else {
                var replacement = $(data);
                $(document).find('#' + replacement.attr('id')).replaceWith(replacement);
                $(document).trigger('contentAdded', {target: replacement});
                form = replacement.find('.class-info-edit-form');
                ajaxifyCallbacks.submit.defaultCallback(e, form, data);
            }*/
        },
        FormPopupAddCompany: function (e, form, data) {
            var name_id = $(form).attr('id');
            var id = data.id;
            console.log(id);
            console.log(data);
            //var replacement = $(data.html);
            //console.log(replacement);

            e.preventDefault();
            $('.modal').modal('hide');
            var replacement = $(data.html);
            AlertMessage(name_id,id, replacement);
            //console.log( AlertMessage());
            /*if(alert){
                $('.btn-profile-edit-adv:first').before(replacement);
            }*/


        },
        FormPopupEditCompany: function (e, form, data) {
            var name_id = $(form).attr('id');
            var id = data.id;
            console.log(id);
            console.log(data);
            e.preventDefault();
            $('.modal').modal('hide');
            var replacement = $(data.html);
            AlertMessage(name_id,id, replacement);
            //ajaxifyCallbacks.ajax.FormPopupEditCompany(e, form, data);

            /*
            //var replacement = $(data.html);
            //console.log(replacement);

            e.preventDefault();
            $('.modal').modal('hide');
            var replacement = $(data.html);
            AlertMessage(name_id,id, replacement);*/
            //console.log( AlertMessage());
            /*if(alert){
                $('.btn-profile-edit-adv:first').before(replacement);
            }*/


        }
    },
    ajax: {
      //Default AJAX callback
      defaultCallback: function(e, form, data) {
        //alert('AJAX default');

      },
      FormPopupEditCompany: function(e, form, data) {
        //alert('AJAX default');
          console.log(funcSerializeFormToObject($('#'+form)));
          var forms = $('#'+form);
          $.ajax({
                url: forms.attr('action'),
                data: funcSerializeFormToObject(forms),
                method: 'post',
                contentType: false,
                processData: false,
                success: function(datas) {
                  //Trigger submit callback
                  console.log(datas);
                }
              });
      }
    },
    //Validate callbacks
    validate: {
      //Default validate callback. Return TRUE if there is some errors
      defaultCallback: function(e, form, values) {
        return false;
      },
      //Login/Register validate function (THIS CALLBACK NOT USED IN AJAXIFY FORM)
      loginRegisterCallback: function(e, form, values) {
        var errors = false;
        form.find('.required').each(function(){
          $(this).removeClass('error').attr('placeholder', $(this).data('placeholder'));
          if(!$(this).val()) {
            errors = true;
            $(this).addClass('error').attr('placeholder', 'Заполните поле');
            if(!$(this).hasClass('validate-processed')) {
              $(this).focus(function(){
                $(this).removeClass('error').attr('placeholder', $(this).data('placeholder'));
              });
              $(this).addClass('validate-processed')
            }
          }
        });
        return errors;
      }
    }
  };
}();

var actionButtonsCallbacks = function () {
  return {
    keyUp:{
      //Default click callback
      defaultCallbacks: function() {
        alert('defaufglt');
      },
    },
    click: {
      //Default click callback
      defaultCallback: function(e, trigger) {
        alert('default');
      },
      // Page Company edit products
      ServEditServiceDelete: function(e, trigger) {
        //alert('default');
          var form = $('#FormPopupEditCompany');
          var action = form.attr('action');
          var id =  $(form.find('[name="id"]')).val();

          AlertMessage('FormPopupEditCompany_DEL', id, action);

      },
      // Page Company edit products
      ServEditPositionSidebar: function(e, trigger) {
          var id = $(trigger).data('id');
          var form = $('#ServEditPositionSidebar');
          $.get(location.href, {type: "json", value: 'product', product: id}, function(data){
             handler(data)
          });
          function handler(data){
              console.log(data);
              console.log(data.name);
              form.find('[name="id"]').val(id);
              form.find('[name="name"]').val(data.name).removeClass('empty');
              form.find('[name="price_from"]').val(data.price_from).removeClass('empty');
              if(data.price_to){
                 form.find('[name="price_to"]').val(data.price_to).removeClass('empty');
              }
              if(data.url){
                 form.find('[name="url"]').val(data.url).removeClass('empty');
              }
              if(data.description){
                 form.find('[name="description"]').val(data.description).removeClass('empty');
              }
              if(data.image){
                  $('#output').attr('src', data.image)
              }
              //form.find('#inputTextTitle').val(data.name);
          }


      },
      // Page billing show more
      BillingOrdersPageShowMore: function(e, trigger) {
          console.log(e, trigger);
          var count = $('.table-orders').find('tr').length;

          $.get(location.href, {type: "order", count: count}, function(data){
             handler(data)
          });
          function handler(data){
              var last = data[data.length - 1].last;
              if(!last){$(trigger).remove()}
              data.forEach(function(item, i) {
                  if(item.pk){
                  var template = '<tr><td>'+item.pk+'</td> <td>'+item.date_at+'</td> <td>'+item.time_at+'</td> <td>+ '+item.amount+' руб.</td> <td> <span data-plugin="peityBar" style="display: none;">2,3,2,-1,-3,-2,2,3,5,4</span><svg class="peity" height="22" width="44"><rect fill="#a2caee" x="0.44000000000000006" y="8.25" width="3.52" height="5.5"></rect><rect fill="#a2caee" x="4.840000000000001" y="5.5" width="3.5199999999999987" height="8.25"></rect><rect fill="#a2caee" x="9.24" y="8.25" width="3.5199999999999996" height="5.5"></rect><rect fill="#a2caee" x="13.64" y="13.75" width="3.5199999999999996" height="2.75"></rect><rect fill="#a2caee" x="18.04" y="13.75" width="3.520000000000003" height="8.25"></rect><rect fill="#a2caee" x="22.439999999999998" y="13.75" width="3.520000000000003" height="5.5"></rect><rect fill="#a2caee" x="26.839999999999996" y="8.25" width="3.5200000000000067" height="5.5"></rect><rect fill="#a2caee" x="31.24" y="5.5" width="3.5200000000000067" height="8.25"></rect><rect fill="#a2caee" x="35.64" y="0" width="3.520000000000003" height="13.75"></rect><rect fill="#a2caee" x="40.04" y="2.75" width="3.520000000000003" height="11"></rect></svg> </td> </tr>';
                  $('.table-orders').append(template)
                  }
              });
          }
      },
      // Page billing show more
      BillingDebitsPageShowMore: function(e, trigger) {
          var count = $('.table-debits').find('tr').length;

          $.get(location.href, {type: "debit", count: count}, function(data){
             handler(data)
          });
          function handler(data){
              console.log(data);
              var last = data[data.length - 1].last;
              if(!last){$(trigger).remove()}
              data.forEach(function(item, i) {
                  if(item.pk){
                      var template = '<tr> <td>'+item.pk+'</td> <td>'+item.date_at+'</td> <td>'+item.time_at+'</td> <td><a href="#">Заявка №'+item.leads.pk+'</a></td> <td>- '+item.amount+' руб.</td> <td> <span data-plugin="peityBar" style="display: none;">2,3,2,-1,-3,-2,2,3,5,4</span><svg class="peity" height="22" width="44"><rect fill="#a2caee" x="0.44000000000000006" y="8.25" width="3.52" height="5.5"></rect><rect fill="#a2caee" x="4.840000000000001" y="5.5" width="3.5199999999999987" height="8.25"></rect><rect fill="#a2caee" x="9.24" y="8.25" width="3.5199999999999996" height="5.5"></rect><rect fill="#a2caee" x="13.64" y="13.75" width="3.5199999999999996" height="2.75"></rect><rect fill="#a2caee" x="18.04" y="13.75" width="3.520000000000003" height="8.25"></rect><rect fill="#a2caee" x="22.439999999999998" y="13.75" width="3.520000000000003" height="5.5"></rect><rect fill="#a2caee" x="26.839999999999996" y="8.25" width="3.5200000000000067" height="5.5"></rect><rect fill="#a2caee" x="31.24" y="5.5" width="3.5200000000000067" height="8.25"></rect><rect fill="#a2caee" x="35.64" y="0" width="3.520000000000003" height="13.75"></rect><rect fill="#a2caee" x="40.04" y="2.75" width="3.520000000000003" height="11"></rect></svg> </td> </tr>';
                      $('.table-debits').append(template)
                  }
              });
          }
      }
    },
    ajax: {
      //Default AJAX callback
      defaultCallback: function(e, trigger, data) {
        //alert('AJAX default');
        console.log(e, trigger, data);
      }
    }
  };
}();


(function($) {

  $.extend($.fn, {
    //Good serialization of form for AJAX
    serializeFormToObject: function(){
        var $form    = this,
                formData = new FormData(),
                params   = $form.serializeArray(),
                files    = $form.find('[name="image"]')[0].files;

        $.each(files, function(i, file) {
                formData.append('image', file);
        });

        $.each(params, function(i, val) {
                formData.append(val.name, val.value);
        });

      return formData;
    },
    //AJAX Form
    ajaxifyForm: function(){
      $(this).each(function(){
        var form = $(this);
        if(!form.hasClass('ajax-processed')) {
          form.submit(function(e) {
            e.preventDefault();
            var submitCallback = form.data("submit") && $.isFunction(ajaxifyCallbacks.submit[form.data("submit")]) ? ajaxifyCallbacks.submit[form.data("submit")] : ajaxifyCallbacks.submit.defaultCallback,
                validateCallback = form.data("validate") && $.isFunction(ajaxifyCallbacks.validate[form.data("validate")]) ? ajaxifyCallbacks.validate[form.data("validate")] : ajaxifyCallbacks.validate.defaultCallback,
                errors = false, values = form.serializeFormToObject(),
                url = form.attr('action') ?  form.attr('action') : window.location.href,
                method = form.attr('method') ? form.attr('method').toLowerCase() : 'get';
            //Check form for errors
            errors = validateCallback(e, form,  values);
            //If NO ERRORS than SEND form
            if(!errors) {
              $.ajax({
                url: url,
                data: values,
                method: method,
                contentType: false,
                processData: false,
                success: function(data) {
                  //Trigger submit callback
                  submitCallback(e, form, data);
                }
              });
            }
          });
          form.addClass('ajax-processed');
        }
      });

      return this;
    },
    actionButton: function(){
      $(this).each(function(){
        var trigger = $(this);
        if(!trigger.hasClass('action-processed')) {
          var url = trigger.attr('href') && trigger.attr('href') != '#' ? trigger.attr('href') : trigger.data('href'), callback = actionButtonsCallbacks.click.defaultCallback, callbackText = trigger.data("callback"),
              method = trigger.data('method') ? trigger.data('method').toLowerCase() : 'get',
              ajaxing_cont = trigger.data('ajaxcont') ? $(trigger.data('ajaxcont')) : trigger;
          if(url && url != '#' && !trigger.data('noajax')) {
            //ajaxCallback
            callback = callbackText && $.isFunction(actionButtonsCallbacks.ajax[callbackText]) ? actionButtonsCallbacks.ajax[callbackText] : actionButtonsCallbacks.ajax.defaultCallback;
            trigger.click(function(e) {
              ajaxing_cont.addClass('ajaxing');
              e.preventDefault();
              $.ajax({
                url: url,
                data: null,
                method: method,
                success: function(data) {
                  ajaxing_cont.removeClass('ajaxing').addClass('ajaxingOut');
                  setTimeout(function(){ ajaxing_cont.removeClass('ajaxingOut'); }, 400);
                  //Trigger success callback
                  callback(e, trigger, data);
                }
              });
            });

          } else {
            //simpleCallback
            callback = callbackText && $.isFunction(actionButtonsCallbacks.click[callbackText]) ? actionButtonsCallbacks.click[callbackText] : actionButtonsCallbacks.click.defaultCallback;
            trigger.click(function(e) {
              e.preventDefault();
              callback(e, trigger);
            });
          }
          trigger.addClass('action-processed');
        }
      });

      return this;
    },





  });



  //Custom event for AJAX loaded content
  $(document).on('contentAdded', function(e, data){
    var target = data.target;

    //AJAX forms
    target.find('.ajaxify-form').ajaxifyForm();

    //Action Buttons
    target.find('.action-button').actionButton();



  });

  $(document).ready(function(){

    //Event trigget on DOM ready
    $(this).trigger('contentAdded', {target: $('html')});

    //Styler on login/register init
    if($('.block-login .checkbox').length) {
      $('.block-login input[type="checkbox"]').styler();
    }

    $('.block-login form').each(function(){
      var form = $(this);
      form.submit(function(e){
        var errors = ajaxifyCallbacks.validate.loginRegisterCallback(e, form, null);
        if(errors) { e.preventDefault(); }
        else {
          //Doing some awesome things ^_^ with form
        }
      });
    });

      //page leads sumernote realtime
      $('.sumernote-realtime').blur(function(){
          alert('Элемент foo потерял фокус.');
      });



  });
})(jQuery);